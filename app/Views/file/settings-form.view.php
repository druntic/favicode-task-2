<div class="options-container hidden" id="options-container-<?php echo $file->id; ?>">
    <form action="/files/<?php echo $file->id; ?>" method="post">
        <input type="hidden" name="updateToken" value="<?php echo $_SESSION['updateToken'] ?? '' ?>">
        <div class="form-group">
            <label for="newFileName">File Name</label>
            <input type="text" class="form-control" id="newFileName" name="newFileName" value="<?php echo $file->name; ?>">
        </div>
        <div class="form-group">
            <label for="newFileType">File Type</label>
            <select class="form-control" id="newFileType" name="newFileType">
                <option value="public">Public</option>
                <option value="private">Private</option>
            </select>
        </div>
        <button type="submit" class="btn btn-success">Save</button>
        <button type="button" class="btn btn-secondary" onclick="toggleOptions('<?php echo $file->id; ?>')">Cancel</button>
    </form>
</div>