<body>
    <?php include 'navigation.view.php'; ?>

    <?php if (!empty($errors)) : ?>
        <div class="alert alert-danger text-center" style="width: 80%; margin: 0 auto;">
            <?php echo $errors[0] ?>
        </div>
    <?php endif; ?>
    <?php if (!empty($update)) : ?>
        <div class="alert alert-success text-center" style="width: 80%; margin: 0 auto;">
            <?php echo $update ?>
        </div>
    <?php endif; ?>
    <?php if (count($files) > 0) : ?>
        <div style="text-align: center;">
            <table class="table" style="width: 80%;">
                <thead>
                    <tr>
                        <th>File Name</th>
                        <th>Size</th>
                        <th>Type</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($files as $file) : ?>

                        <tr style="background-color: #f2f2f2;">
                            <td style="white-space: nowrap;" onclick="openPopup('<?php echo $file->id; ?>', '<?php echo $file->name; ?>')"><?php echo $file->name; ?></td>
                            <td><?php echo $file->size / 1000; ?> KB</td>
                            <td><?php echo $file->type; ?></td>
                            <td> <?php include __DIR__ . '/file/file-buttons.view.php'; ?></td>
                            <td> <?php include __DIR__ . '/file/settings-form.view.php'; ?></td>
                        </tr>

                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>



        <style>
            .hidden {
                display: none;
            }

            .show-options {
                display: block;
            }
        </style>
    <?php else : ?>
        <p>No files found.</p>
    <?php endif; ?>
</body>