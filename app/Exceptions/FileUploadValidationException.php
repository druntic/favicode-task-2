<?php

namespace app\Exceptions;

use Exception;


class FileUploadValidationException extends Exception
{
    public function __construct($message, $code = 403, $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
