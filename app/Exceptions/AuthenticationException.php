<?php

namespace app\Exceptions;
use Exception;


class AuthenticationException extends Exception {
    public function __construct($message, $code = 401, $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}