<?php

namespace app\Exceptions;

use Exception;


class UserExistsException extends Exception
{
    public function __construct($message, $code = 409, $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
