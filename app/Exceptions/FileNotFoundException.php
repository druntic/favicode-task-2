<?php

namespace app\Exceptions;

use Exception;


class FileNotFoundException extends Exception
{
    public function __construct($message, $code = 404, $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
