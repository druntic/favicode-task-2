<?php

namespace app\Exceptions;

use Exception;


class StorageLimitExceededException extends Exception
{
    public function __construct($message, $code = 403, $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
