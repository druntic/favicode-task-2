<?php

namespace app\Services;

use app\Exceptions\AuthenticationException;
use app\Exceptions\FileNotFoundException;
use app\Interfaces\FileInterface;
use app\Models\File;
use app\Exceptions\StorageLimitExceededException;
use Exception;

class FileService implements FileInterface
{
    public function getUserFiles($userId)
    {
        return File::getUserFiles($userId);
    }

    public function updateFile($fileId, $fileName, $fileType)
    {
        $file = File::getFileById($fileId);
        //check if logged in user has permission to update
        if ($file->user_id != $_SESSION["user"]["id"]) throw new AuthenticationException("File could not be updated");
        File::updateFileById($fileId, ["name" => $fileName, "type" => $fileType]);
    }

    public function upload($user, $uploadedFile, File $file)
    {
        //create file in storage/userId directory
        $filePath = STORAGE_DIR . $user['id'];
        //check if directory already exists and if not then create it
        if (!file_exists($filePath)) mkdir($filePath, 0777, true);
        //hash the file name. More secure and allows files with same names to be uploaded
        $storedName = hash('sha256', $uploadedFile['name'] . bin2hex(random_bytes(8)))
            . "." . pathinfo($uploadedFile['name'])['extension'];
        //check if user has enough storage to upload file
        $maxUserStorage = $user['allowedStorageGB'] * 1000000000; //convert gigabytes to bytes
        $userUsedStorage = 0;
        $files = File::getUserFiles($user['id']);
        foreach ($files as $userFile) {
            $userUsedStorage += $userFile->size;
        }
        if ($uploadedFile['size'] + $userUsedStorage > $maxUserStorage) {
            throw new StorageLimitExceededException('You do not have enough storage to upload this file');
        }
        // move file from tmp folder to $filePath
        move_uploaded_file($uploadedFile['tmp_name'], $filePath . '/' . $storedName);

        // create log in database
        $file->storedName = $storedName;
        $file->save();
    }

    public function deleteFile($userId, $fileId)
    {
        $file = File::getFileById($fileId);

        //check if file exists
        if (!$file) throw new FileNotFoundException('File could not be deleted');

        //check if user has permission to delete file
        if ($userId != $file->user_id) throw new AuthenticationException("You do not have permission to delete this file");
        $filePath = STORAGE_DIR . $userId . '/' . $file->storedName;
        //delete file from server
        unlink($filePath);

        //delete file log from database
        File::deleteFile($fileId);
    }

    public function download($userId, $fileId)
    {
        $file = File::getFileById($fileId);
        //check if file exists
        if (!$file) throw new FileNotFoundException('File could not be downloaded');
        //check if user has permission to delete file
        if ($userId != $file->user_id && $file->type == 'private') throw new AuthenticationException("You do not have permission to download this file");
        $filePath = STORAGE_DIR . $file->user_id . '/' . $file->storedName;
        //tells the broswer to download the file
        header('Content-Disposition: attachment; filename="' . $file->name . '"');
        readfile($filePath);
    }
}
