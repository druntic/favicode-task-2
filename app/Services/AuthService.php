<?php

namespace app\Services;

use app\Exceptions\AuthenticationException;
use app\Exceptions\UserExistsException;
use app\Interfaces\AuthInterface;
use app\Models\User;

class AuthService implements AuthInterface
{

    public function store($email, $name, $password)
    {
        //check if user with email already exists
        $user = User::getUserbyEmail($email);
        if (!empty($user)) throw new UserExistsException("Invalid credentials");
        $password_hashed = password_hash($password, null);
        //create new user
        $user = new User();
        $user->insert(["password" => $password_hashed, "email" => $email, "name" => $name]);
        //create folder for new user
        $userId = $user->first(["email" => $email])->id;
        mkdir(STORAGE_DIR . $userId, true);
    }

    public function authenticate($email, $password)
    {
        $user = User::getUserbyEmail($email);
        //check if user with email exists
        if (empty($user)) throw new UserExistsException("Invalid credentials");
        //check if passwords match
        if (!password_verify($password, $user->password)) throw new AuthenticationException("Invalid credentials");
        return ["id" => $user->id, "name" => $user->name, "email" => $user->email, "allowedStorageGB" => $user->allowedStorageGB];
    }
}
