<?php

namespace app\Interfaces;


interface AuthInterface
{
    public function authenticate($email, $password);
    public function store($email, $name, $password);
}
