<?php

use app\Exceptions\AuthenticationException;
use app\Exceptions\UserExistsException;
use app\Exceptions\UserValidationLoginException;
use app\Exceptions\UserValidationRegistrationException;
use app\Models\User;
use app\Services\AuthService;


class AuthController extends Controller
{
    private $authService;

    public function __construct(AuthService $service)
    {
        $this->authService = $service;
    }

    public function index()
    {
        try {
            isLoggedOut();
            $data['errors'] = $_SESSION['errors'] ?? [];
            $_SESSION['loginToken'] = md5(uniqid(mt_rand(), true));
            unset($_SESSION['errors']); // clear session errors after retrieving them
            $this->view('auth/login', $data);
        } catch (Exception $e) {
            return $this->view('errors/500');
        }
    }
    public function register()
    {
        try {
            isLoggedOut();
            $data['errors'] = $_SESSION['errors'] ?? [];
            $_SESSION['token'] = md5(uniqid(mt_rand(), true));
            unset($_SESSION['errors']); // clear session errors after retrieving them
            $this->view('auth/register', $data);
        } catch (Exception $e) {
            return $this->view('errors/500');
        }
    }

    public function store()
    {
        try {
            $email = $_POST['email'];
            $name = $_POST['name'];
            $password = $_POST['password'];
            $sessionToken = $_SESSION['registerToken'];
            $formToken = $_POST['registerToken'];
            //validate form data
            User::validateUserRegistration($email, $name, $password, $sessionToken, $formToken);
            //create user
            $this->authService->store($email, $name, $password);
            return redirect('/login');
        } catch (UserValidationRegistrationException $e) {
            // save validation errors in session
            $_SESSION['errors'] = [$e->getMessage()];
            return redirect('/register');
        } catch (UserExistsException $e) {
            $_SESSION['errors'] = [$e->getMessage()];
            return redirect('/register');
        }
        catch (AuthenticationException $e) {
            $_SESSION['errors'] = [$e->getMessage()];
            return redirect('/register');
        } catch (Exception $e) {
            return $this->view('errors/500');
        }
    }

    public function authenticate()
    {
        try {
            $email = $_POST["email"];
            $password = $_POST["password"];
            $sessionToken = $_SESSION['loginToken'];
            $formToken = $_POST['loginToken'];
            //validate form data
            User::validateUserLogin($email, $password, $sessionToken, $formToken);
            //try to authenticate user and return user id, name, email and allowedStorageGB if success
            $user = $this->authService->authenticate($email, $password);
            $_SESSION['user'] = $user;
            return redirect('/');
        } catch (UserValidationLoginException $e) {
            $_SESSION['errors'] = [$e->getMessage()];
            return redirect('/');
        } catch (AuthenticationException $e) {
            $_SESSION['errors'] = [$e->getMessage()];
            return redirect('/');
        } catch (UserExistsException $e) {
            $_SESSION['errors'] = [$e->getMessage()];
            return redirect('/');
        } catch (Exception $e) {
            return $this->view('errors/500');
        }
    }

    public function logout()
    {
        try {
            session_destroy();
            return redirect('/login');
        } catch (Exception $e) {
            return $this->view('errors/500');
        }
    }
}
