<?php

use app\Exceptions\AuthenticationException;
use app\Exceptions\FileUpdateValidationException;
use app\Models\File;
use app\Services\FileService;
use app\Exceptions\FileUploadValidationException;
use app\Exceptions\StorageLimitExceededException;
use app\Exceptions\FileNotFoundException;

class FileController extends Controller
{


    private $fileService;

    public function __construct(FileService $service)
    {
        $this->fileService = $service;
    }
    public function index()
    {
        try {
            isLoggedIn();
            $data['errors'] = $_SESSION['errors'] ?? [];
            $data['update'] = $_SESSION['update'] ?? [];
            unset($_SESSION['errors']);
            unset($_SESSION['update']);
            $_SESSION['updateToken'] = md5(uniqid(mt_rand(), true));
            $files = $this->fileService->getUserFiles($_SESSION["user"]["id"]);
            $data["files"] = $files ?? [];
            return $this->view('dashboard', $data);
        } catch (Exception $e) {
            return $this->view('errors/500');
        }
    }

    public function update($fileId)
    {
        try {
            isLoggedIn();
            //validate
            File::validateFileUpdate($_POST["newFileName"], $_POST["newFileType"], $_SESSION['updateToken'], $_POST['updateToken']);
            $this->fileService->updateFile($fileId, $_POST["newFileName"], $_POST["newFileType"]);
            $_SESSION['update'] = 'File has been updated';
            return redirect('/');
        } catch (FileUpdateValidationException $e) {
            $_SESSION['errors'] = [$e->getMessage()];
            return redirect('/');
        } catch (AuthenticationException $e) {
            $_SESSION['errors'] = [$e->getMessage()];
            return redirect('/');
        } catch (Exception $e) {
            return $this->view('errors/500');
        }
    }

    public function showUpload()
    {
        try {
            isLoggedIn();
            $_SESSION['uploadToken'] = md5(uniqid(mt_rand(), true));
            return $this->view('file/upload');
        } catch (Exception $e) {
            return $this->view('errors/500');
        }
    }

    public function upload()
    {
        try {
            isLoggedIn();
            $user = $_SESSION['user'];
            $sessionToken = $_SESSION['uploadToken'];
            $formToken = $_POST['uploadToken'];
            //create and validate the file
            $file = new File(
                $_FILES['file']['size'],
                $_POST['type'],
                null,
                $_SESSION['user']['id'],
                $_FILES['file']['name'],
                null,
                $_FILES['file']['error']
            );
            $file->validateFile($sessionToken, $formToken);
            // Upload the file to userId folder
            $this->fileService->upload($user, $_FILES['file'], $file);
            return redirect('/');
        } catch (FileUploadValidationException $e) {
            $_SESSION['errors'] = [$e->getMessage()];
            return redirect('/');
        } catch (StorageLimitExceededException $e) {
            $_SESSION['errors'] = [$e->getMessage()];
            return redirect('/');
        } catch (AuthenticationException $e) {
            $_SESSION['errors'] = [$e->getMessage()];
            return redirect('/');
        }
        catch (Exception $e) {
            return $this->view('errors/500');
        }
    }

    public function delete($fileId)
    {
        try {
            isLoggedIn();
            $this->fileService->deleteFile($_SESSION['user']['id'], $fileId);
            $_SESSION['update'] = 'File has been deleted';
            return redirect('/');
        } catch (FileNotFoundException $e) {
            $_SESSION['errors'] = [$e->getMessage()];
            return redirect('/');
        } catch (AuthenticationException $e) {
            $_SESSION['errors'] = [$e->getMessage()];
            return redirect('/');
        } catch (Exception $e) {
            return $this->view('errors/500');
        }
    }

    public function download($fileId)
    {
        try {
            isLoggedIn();
            $this->fileService->download($_SESSION['user']['id'], $fileId);
        } catch (FileNotFoundException $e) {
            $_SESSION['errors'] = [$e->getMessage()];
            return redirect('/');
        } catch (AuthenticationException $e) {
            $_SESSION['errors'] = [$e->getMessage()];
            return redirect('/');
        } catch (Exception $e) {
            return $this->view('errors/500');
        }
    }
}
