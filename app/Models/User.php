<?php


namespace app\Models;

use app\Exceptions\AuthenticationException;
use Model;
use app\Exceptions\UserValidationRegistrationException;
use app\Exceptions\UserValidationLoginException;
class User
{

    use Model;
    protected $table = 'users';

    // columns which are allowed to be edited
    protected $allowedColumns = [
        'name',
        'email',
        'password'
    ];

    public static function getUserbyEmail($email)
    {
        $user = new User();
        return $user->first(["email" => $email]);
    }

    public function set($key, $value)
    {
        if (in_array($key, $this->allowedColumns)) {
            $this->$key = $value;
        }
    }

    public static function getUserbyId($id)
    {
        $user = new User();
        return $user->first(["id" => $id]);
    }

    public static function validateUserRegistration($email, $name, $password, $sessionToken, $formToken) {
        if (empty($email)) throw new UserValidationRegistrationException('email is required');
        elseif(!filter_var($email, FILTER_VALIDATE_EMAIL)) throw new UserValidationRegistrationException('email is invalid');
        if (empty($name)) throw new UserValidationRegistrationException('name is required');
        if (empty($password)) throw new UserValidationRegistrationException('password is required');
        if (is_null($formToken) || $sessionToken != $formToken) throw new AuthenticationException('You are not allowed to do this action');
    }

    public static function validateUserLogin($email, $password, $sessionToken, $formToken) {
        if (empty($email)) throw new UserValidationLoginException('email is required');
        elseif(!filter_var($email, FILTER_VALIDATE_EMAIL)) throw new UserValidationLoginException('email is invalid');
        if (empty($password)) throw new UserValidationLoginException('password is required');
        if (is_null($formToken) || $sessionToken != $formToken) throw new AuthenticationException('You are not allowed to do this action');
    }


}
