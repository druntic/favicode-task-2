<?php


namespace app\Models;
use Model;
use app\Exceptions\FileUploadValidationException;
use app\Exceptions\FileUpdateValidationException;
use app\Exceptions\AuthenticationException;

class File {
    
    use Model;
    protected $table = 'files';
    // columns which are allowed to be edited
    protected $allowedColumns = [
        'name',
        'type',
        'size',
        'user_id',
        'storedName',
        'id'
    ];

    public $size;
    public $type;
    public $id;
    public $user_id;
    public $name;
    public $storedName;
    public $error;

    public function __construct($size = null, $type = null, $id = null, $user_id = null, $name = null ,$storedName = null, $error = null)
    {
        $this->size = $size;
        $this->type = $type;
        $this->id = $id;
        $this->user_id = $user_id;
        $this->name = $name;
        $this->storedName = $storedName;
        $this->error = $error;
    }

    public static function getUserFiles($userId) {
        $file = new File();
        return $file->where(["user_id" => $userId]);
    }

    public function set($key, $value) {
        if (in_array($key, $this->allowedColumns)) {
            $this->$key=$value;
        }
    }

    public static function getFileById($fileId) {
        $file = new File();
        return $file->first(["id" => $fileId]);
    }

    public static function updateFileById($fileId, $data) {
        $file = new File();
        $file->update($fileId, $data);
    }

    public static function deleteFile($fileId) {
        $file = new File();
        $file->delete($fileId);
    }

    public function validateFile($sessionToken, $formToken) {
        if ($this->size> UPLOAD_MAX_SIZE_MB * 1024 * 1024) throw new FileUploadValidationException("File cannot be bigger than 40MB");
        if ($this->error != 0) throw new FileUploadValidationException("Error " . $this->error);
        if ($this->size == 0 || $this->size == null) throw new FileUploadValidationException("Invalid file");
        if ($this->name == "" || $this->name == null) throw new FileUploadValidationException("Invalid file name");
        if ($this->type != "private" && $this->type !="public") throw new FileUploadValidationException("Invalid file type");
        if (is_null($formToken) || $sessionToken != $formToken) throw new AuthenticationException('You are not allowed to do this action');
    }

    public static function validateFileUpdate($fileName, $fileType, $sessionToken, $formToken) {
        if (is_null($fileName)) throw new FileUpdateValidationException("Invalid file name");
        if ($fileType != "private" && $fileType !="public") throw new FileUpdateValidationException("Invalid file type");
        if (is_null($formToken) || $sessionToken != $formToken) throw new AuthenticationException('You are not allowed to do this action');
    }
}