<?php

class App
{
    private function splitURL()
    {
        // check if there is url key, if not set URl to ""
        $URL = $_GET["url"] ?? "";
        $URL = explode("/", $URL);

        return $URL;
    }

    public function loadController($container)
    {
        $URL = $this->splitURL();
        $method = $URL[0] ?? "index";
        //php does not have http methods for put, patch and delete. We will do post methods and get a hidden http metod value instead
        $HTTPmethod = $_POST["method"] ?? "POST";

        if ($method == "login" || $method == "register" || $method == "logout") require "../app/Controllers/AuthController.php";
        if ($method == "" || $method == "index" || $method == "files" || $method == "upload") require "../app/Controllers/FileController.php";
        //auth routes
        if ($method == "login" && $_SERVER["REQUEST_METHOD"] == "GET") return $container->get(AuthController::class)->index();
        if ($method == "login" && $_SERVER["REQUEST_METHOD"] == "POST") return $container->get(AuthController::class)->authenticate();
        if ($method == "register" && $_SERVER["REQUEST_METHOD"] == "GET") return $container->get(AuthController::class)->register();
        if ($method == "register" && $_SERVER["REQUEST_METHOD"] == "POST") return $container->get(AuthController::class)->store();
        if ($method == "logout" && $_SERVER["REQUEST_METHOD"] == "GET") return $container->get(AuthController::class)->logout();

        //file routes
        if (($method == "" || $method == "index") && $_SERVER["REQUEST_METHOD"] == "GET") return $container->get(FileController::class)->index();
        if ($method == "files" && $_SERVER["REQUEST_METHOD"] == "GET") return $container->get(FileController::class)->download($URL[1]);
        if ($method == "files" && $_SERVER["REQUEST_METHOD"] == "POST") {
            if($HTTPmethod == "POST") return $container->get(FileController::class)->update($URL[1]);
            else if($HTTPmethod == "DELETE") return $container->get(FileController::class)->delete($URL[1]);
        }
        if ($method == "upload" && $_SERVER["REQUEST_METHOD"] == "GET") return $container->get(FileController::class)->showUpload();
        if ($method == "upload" && $_SERVER["REQUEST_METHOD"] == "POST") return $container->get(FileController::class)->upload();

        require "../app/Controllers/_404.php";
        return $container->get(_404::class)->index();
    }
}
