<?php
// connect to database

trait Database {
    private function connect() {
        $string = DB_CONNECTION.":hostname=localhost;dbname=".DB_DATABASE;
        // return connection to db
        return new PDO($string, DB_USERNAME, DB_PASSWORD);
    }

    public function query($query, $data = []) {
        $connection = $this->connect();
        $statement = $connection->prepare($query);
        $check = $statement->execute($data);

        if ($check) {
            $result = $statement->fetchAll(PDO::FETCH_OBJ);
            if (is_array($result) && count($result) ) {
            return $result;
        }
        }
        return [];
    }
}



