<?php
use DI\ContainerBuilder;
use app\Services\AuthService;
use app\Services\FileService;

// load .env variables
require "../vendor/autoload.php";
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__ . "/../..");
$dotenv->load();

// load db values from env
define('DB_CONNECTION', $_ENV['DB_CONNECTION']);
define('DB_DATABASE', $_ENV['DB_DATABASE']);
define('DB_USERNAME', $_ENV['DB_USERNAME']);
define('DB_PASSWORD', $_ENV['DB_PASSWORD']);

define('DEBUG', $_ENV['DEBUG']);
define('HOST', $_ENV['HOST']);
// enables _POST to get json content(used for Postman)
//$_POST = json_decode(file_get_contents("php://input"), true);


//Dependency injection
$containerBuilder = new ContainerBuilder();
$container = $containerBuilder->build();
$container->set(AuthService::class, DI\autowire());
$container->set(FileService::class, DI\autowire());

//storage options
define('STORAGE_DIR', __DIR__."/../storage/userFiles/");
//go to php.ini if you want to change this to a custom value
define('UPLOAD_MAX_SIZE_MB', 40);
