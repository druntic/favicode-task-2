<?php

class Controller{

    public function view($name, $data = []){

        if(!empty($data)) extract($data);

        $fileName = "../app/Views/".$name.".view.php";
        if(file_exists($fileName)) {
            require $fileName;
        }
        else {
            require "../app/Views/404.view.php";
        }
    }

}