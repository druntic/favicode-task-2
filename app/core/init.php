<?php
//this function only runs if php runs a class which it cant find
spl_autoload_register(function($className) {
    //load models
    require "../" .$className.".php";
});
require "config.php";
require "functions.php";
require "Database.php";
require "Model.php";
require "Controller.php";
require "App.php";