<?php

function show($item) {
    echo "<pre>";
    print_r($item);
    echo "<pre>";
}

function redirect($url) {
    // when the browser gets the location header in response it tries to redirect to location
    header("Location: " . $url);
    exit();
}


//TODO: create middleware using this  function
function isLoggedIn() {
    if(empty($_SESSION["user"])) {
        header("Location: /login" );
        exit();
    }
}

function isLoggedOut() {
    if(!empty($_SESSION["user"])) {
        header("Location: /" );
        exit();
    }
}