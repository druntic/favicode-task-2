<?php
use Ramsey\Uuid\Uuid;
trait Model {
    
    use Database;
    // get data using some conditions from $data
    public function where($data) {
        $data = $this->removeUnwantedData($data);
        $keys = array_keys($data);
        $query = "select * from $this->table where ";
        foreach($keys as $key) {
            $query .= $key . " =:" . $key . " && ";
        }
        $query = trim($query, " && ");
        //TODO: add query options for sorting, limiting and filtering
        return $this->query($query, $data);
    }
    // get only first value. needs to be updated to create a better query which does not get all results
    public function first($data) {
        $result = $this->where($data);
        if ($result){
            return $result[0];
        }
        return false;
    }
    // insert into table
    public function insert($data) {
        $data = $this->removeUnwantedData($data);
        // add id as key
        $data["id"] = Uuid::uuid4();
        $keys = array_keys($data);
        $query = "insert into $this->table (". implode(",", $keys) .") values (:" . implode(",:", $keys) . ")";
        return $this->query($query, $data);
    }

    public function update($id, $data) {
        $data = $this->removeUnwantedData($data);
        $data['id'] = $id;
        $keys = array_keys($data);
        $query = "update $this->table set ";
        foreach($keys as $key) {
            $query .= $key . " =:" . $key . ", ";
        }
        $query = trim($query, ", ");
        $query .= " where id =:id";
        $this->query($query, $data);
        return false;
    }

    public function delete($id) {
        $data = ['id' => $id];
        $query = "delete from $this->table where id =:id";
        $this->query($query, $data);
        return false;
    }

    public function findAll() {
        $query = "select * from $this->table";
        return $this->query($query);
    }


    private function removeUnwantedData($data) {
        //remove unwanted data that user inputted
        $allowedColumns = $this->allowedColumns;
        $filteredData = [];
        if(!empty($allowedColumns)) {
            foreach($data as $key => $value) {
                if(in_array($key, $allowedColumns)) {
                    $filteredData[$key] = $value;
                }
            }
        }
        return $filteredData;
    }

    // save object in table
    public function save() {
        $data = (array) $this;
        $data = $this->removeUnwantedData($data);
        // add id as key
        $data["id"] = Uuid::uuid4();
        $keys = array_keys($data);
        $query = "insert into $this->table (". implode(",", $keys) .") values (:" . implode(",:", $keys) . ")";
        return $this->query($query, $data);
    }
}