# Description

A replica of [favicode-task-david](https://gitlab.com/druntic/favicode-task-david) using no frameworks.


## Installation

- Clone this repository or download the zip file.
- Copy the .env.example file to a new file named .env and configure to your specifications:
- Run `composer install` to install the dependencies.
- Create a database called `faviDB` in phpmyadmin and import `faviDB.sql`
- Configure `Apache` to run `app/public/index.php`
